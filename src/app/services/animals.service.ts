import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AnimalList} from '../models/animal-list';
import {Animal} from '../models/animal';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  constructor(private http: HttpClient) {}

  apiURL = 'http://192.168.88.20:9000/api/animals/';

  getAnimalsIsList() {
    return this.http.get<AnimalList>(this.apiURL);
  }

  getAnimalDetails(id: number) {
    return this.http.get<Animal>(
      this.apiURL + id
  );
  }
}
