import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Animal} from '../models/animal';
import {User} from '../models/user';
import {UserList} from '../models/user-list';

@Injectable({
  providedIn: 'root'
})
// this service is for testing users backend service from previous training - Spring Data
export class UsersService {

  constructor(private http: HttpClient) { }
  // path set for url where backend sent JSON data
  apiUrlList = 'http://localhost:8080/user/list';
  apiUrlSingleUser = 'http://localhost:8080/user/';

  /**
  look into .get description - 'Basic ..' is show in Postman (authorization,
  Preview is clicked, headers values in authorization
  */
  getUser(id: string) {
    return this.http
      .get<User>(this.apiUrlSingleUser + id, {
        headers: {'Authorization': 'Basic TmFtZV91c2VyMTpQYXNzd29yZF91c2VyMQ=='}
      });
  }

  getUsersAsList() {
    return this.http
      .get<UserList>(this.apiUrlList, {
        headers: {'Authorization': 'Basic TmFtZV91c2VyMTpQYXNzd29yZF91c2VyMQ=='}
      });
  }
}
