import {Component, OnInit} from '@angular/core';
import {AnimalsService} from './services/animals.service';
import {forEach} from '@angular/router/src/utils/collection';
import {AnimalList} from './models/animal-list';
import {Animal} from './models/animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
