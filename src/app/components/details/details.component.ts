import { Component, OnInit } from '@angular/core';
import {AnimalsService} from '../../services/animals.service';
import {Animal} from '../../models/animal';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private animalsService: AnimalsService, private route: ActivatedRoute) { }

  animal: Animal = new Animal();

  ngOnInit() {
    // taking id from url - we pack it into number because in impl it gives string
    const  id = Number(this.route.snapshot.paramMap.get('id'));
    // show result base on id
    this.animalsService.getAnimalDetails(id).subscribe( result  => (
      this.animal = result
    ));
  }
}
