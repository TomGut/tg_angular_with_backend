import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {User} from '../../models/user';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private userService: UsersService, private route: ActivatedRoute) { };
  id = this.route.snapshot.paramMap.get('id');

  user: User = new User();

  ngOnInit() {
    return this.userService.getUser(this.id).subscribe(result => (
      this.user = result
    ));
  }
}
