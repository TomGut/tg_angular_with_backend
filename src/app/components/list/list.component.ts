import { Component, OnInit } from '@angular/core';
import {AnimalsService} from '../../services/animals.service';
import {Animal} from '../../models/animal';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private animalsService: AnimalsService) {}

  animals: Animal[] = [];

  ngOnInit(): void {
    this.animalsService.getAnimalsIsList().subscribe(animalsAsList => {
      this.animals = animalsAsList.Results;
    });
  }

}
