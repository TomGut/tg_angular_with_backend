import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DetailsComponent } from './components/details/details.component';
import {RouterModule, Routes} from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { UsersComponent } from './components/users/users.component';
import { UserListComponent } from './components/user-list/user-list.component';

const appRoutes: Routes = [
  // id param (:id) for details module
  {path: 'details/:id', component: DetailsComponent, pathMatch: 'full'},
  // details for list
  {path: '', component: UserListComponent, pathMatch: 'full'},
  {path: 'user/list', component: UserListComponent, pathMatch: 'full'},
  {path: 'user/:id', component: UsersComponent, pathMatch: 'full'}
] ;

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    ListComponent,
    UsersComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
