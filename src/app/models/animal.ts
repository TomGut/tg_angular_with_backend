export class Animal {
  Age: number;
  Id: number;
  IsAdopted: boolean;
  Name: string;
  PhotoUrl: string;
  ShortDescription: string;
}
